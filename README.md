# Luuna Backend Test

[GraphQL API](https://luuna-backend-test.uc.r.appspot.com/graphql) to managa a product catalog.

## Running the project

Install NPM dependencies by `npm install`.
Create a _.env_ file in the base directory, and include the following contents:
```
NODE_ENV=develop
GOOGLE_CLOUD_PROJECT=luuna-backend-test
GOOGLE_APPLICATION_CREDENTIALS=gcp-sa.json
```

Get a GCP Service Account Key file from @BernardoLaing, and save it as _gcp-sa.json_ in the base directory.

Copy the _example.ormconfig.json_ file to _ormconfig.json_, and update it with credentials to access a local/cloud database.
Build the project, and run migrations by:
```
npm run build
npx typeorm migration:run
```

Now you can start using the project with `npm run start`.

## Project structure

The project is divided into 3 modules: Users, Catalog, and Shared.

### Users

The users module handles CRUD operations for Users, as well as Authentication.

### Catalog

The catalog module handles CRUD operations for Products. Brand and Category Management is pending. 

```
The Product Model has an event listener, to handle each action (create, update, or delete) accordingly and notify any admin users.
```

### Shared

The shared module contains utilities, generic classes, and services which work independently of any other module.

## Database

The project uses a PostgresSQL DB, due to the nature of how the product catalog and it's functionalities could evolve. This module currently has only a couple of relationships, but as functionality increases, the schema could become more complex, and we are trying to avoid common issues in NoSQL databases, such as duplicated information, which in turn can become harder to update. Due to the expected volume of the product catalog (Assuming max ~50 brands, with max ~200 products each, resulting in 10,000 product records.) we foresee no issues in database scaling in the near future.

## Logging

In order to better understand how potential customers feel about our products, and which are of interest, we log every query made for a specific product by an anonymous user. To mitigate the impact on the catalog database, these logs are being stored in an ElasticSearch cluster, which also provides an off-the-shelf solution to generate reports on the information collected here, by means of Kibana. The decision to store this logs in a different location from the catalog database stems from the fact that, over time, there could be hundreds of thousands of records, and the constant writes and memory-intensive queries needed to extract meaningful information from them, could result in a higher need for resources on the DB server, and longer wait times for API consumers.

### Example of Logging reports

![Individual Product Queries](./ProductSearches.png "Individual Product Queries")

## Notifications

Notifications are handled by the notifications service, in the shared module. As of now, notifications are sent by email, using SendGrid's API.

## Deployment

The API is hosted on GCP, due to its ease of use, configuration, and scalability. 
