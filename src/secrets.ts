import { SecretManagerServiceClient } from '@google-cloud/secret-manager';

export const loadSecrets = async (): Promise<void> => {
  const client = new SecretManagerServiceClient();

  const [secrets] = await client.listSecrets({ pageSize: 0, parent: `projects/${process.env.GOOGLE_CLOUD_PROJECT}` });
  await Promise.all(
    secrets.map(async (secret) => {
      const [version] = await client.accessSecretVersion({
        name: `${secret.name}/versions/latest`,
      });
      const paths = secret.name?.split('/');
      if (paths) {
        const secretName = paths[3];
        process.env[secretName] = version?.payload?.data?.toString();
      }
    })
  );

  await client.close();
};
