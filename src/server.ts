import { ApolloServer } from 'apollo-server-express';
import * as GraphiQL from 'apollo-server-module-graphiql';
import * as express from 'express';
import { Request, Response } from 'express';
import { execute, subscribe } from 'graphql';
import { createServer, Server } from 'http';
import 'reflect-metadata';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { ApolloServerLoaderPlugin } from 'type-graphql-dataloader';
import { createConnection, getConnection } from 'typeorm';
import * as url from 'url';
import { getUserFromJWT } from './auth';
import { createSchema } from './schema';

type ExpressGraphQLOptionsFunction = (req?: Request, res?: Response) => unknown | Promise<unknown>;

/**
 * graphiqlExpress
 * @description GraphQL configuration
 * @param options
 * @returns arrow function
 */
function graphiqlExpress(options: GraphiQL.GraphiQLData | ExpressGraphQLOptionsFunction) {
  const graphiqlHandler = (req: Request, res: Response, next: CallableFunction) => {
    const query = req.url && url.parse(req.url, true).query;
    GraphiQL.resolveGraphiQLString(query, options, req).then(
      (graphiqlString: string) => {
        res.setHeader('Content-Type', 'text/html');
        res.write(graphiqlString);
        res.end();
      },
      (error: Error) => next(error)
    );
  };
  return graphiqlHandler;
}

/**
 * createConnection
 * @description server configuration
 * @param port
 * @returns Server
 */
export default async (port: number): Promise<Server> => {
  await createConnection();
  const app = express();
  const server: Server = createServer(app);

  const schema = await createSchema();
  const apolloServer = new ApolloServer({
    playground: process.env.NODE_ENV !== 'production',
    introspection: process.env.NODE_ENV !== 'production',
    plugins: [
      ApolloServerLoaderPlugin({
        typeormGetConnection: getConnection,
      }),
    ],
    schema,
    context: async ({ req }: { req: Request }) => {
      const jwt = req.headers?.authorization?.substr(7);
      if (jwt) {
        const user = await getUserFromJWT(jwt);
        return {
          req,
          user,
        };
      }
      return { req };
    },
  });

  apolloServer.applyMiddleware({ app, path: '/graphql' });

  if (module.hot) {
    app.use(
      '/graphiql',
      graphiqlExpress({
        endpointURL: '/graphql',
        query:
          '# Welcome to your own GraphQL server!\n#\n' +
          '# Press Play button above to execute GraphQL query\n#\n' +
          '# You can start editing source code and see results immediately\n\n' +
          'query hello($subject:String) {\n  hello(subject: $subject)\n}',
        subscriptionsEndpoint: `ws://localhost:${port}/subscriptions`,
        variables: { subject: 'World' },
      })
    );
  }

  return new Promise<Server>((resolve) => {
    server.listen(port, () => {
      // tslint:disable-next-line
      new SubscriptionServer(
        {
          execute,
          schema,
          subscribe,
        },
        {
          path: '/subscriptions',
          server,
        }
      );
      resolve(server);
    });
  });
};
