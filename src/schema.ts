import { GraphQLSchema } from 'graphql';
import { buildSchema } from 'type-graphql';
import { authChecker } from './auth';
import { ProductResolver } from './catalog/resolvers/product.resolvers';
import { AuthResolvers } from './users/resolvers/auth.resolvers';
import { UserResolvers } from './users/resolvers/user.resolvers';

export const createSchema = async (): Promise<GraphQLSchema> => {
  try {
    return await buildSchema({
      resolvers: [ProductResolver, UserResolvers, AuthResolvers],
      dateScalarMode: 'isoDate', // "2018-02-07T21:04:39.573Z"
      emitSchemaFile: process.env.NODE_ENV === 'develop',
      authChecker,
    });
  } catch (e) {
    console.error('Error building schema: ', e);
    throw e;
  }
};
