import { config } from 'dotenv';
import { Server } from 'http';
import { NotificationService } from './shared/notifications.service';
import { loadSecrets } from './secrets';
import startServer from './server';

let server: Server;
let PORT: number;

async function main() {
  config();
  await loadSecrets();
  PORT = +(process.env.PORT ?? 4000);
  if (process.env.SENDGRID_API_KEY && process.env.SENDGRID_SENDER)
    NotificationService.configure(process.env.SENDGRID_API_KEY, process.env.SENDGRID_SENDER);

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose((data) => {
      if (server) server.close();
      data.hotReloaded = true;
    });
    module.hot.addStatusHandler((status) => {
      if (status === 'fail') {
        process.exit(250);
      }
    });
  }

  server = await startServer(PORT);
}

main()
  .then(() => {
    console.log(`Gordon GraphQL Server is now running on http://localhost:${PORT}/graphql`);
  })
  .catch((error) => {
    console.error('An error has ocurred trying to start the server: ', error);
    process.exit();
  });
