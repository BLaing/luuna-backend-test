import { IsOptional, Length } from 'class-validator';
import { Field, InputType } from 'type-graphql';
import { User } from '../models/user.model';

@InputType({ description: 'Data to create a user.' })
export class UpdateUserInput implements Partial<User> {
  @Field(() => String, { nullable: true })
  @Length(1, 32)
  @IsOptional()
  firstName: string;

  @Field(() => String, { nullable: true })
  @Length(1, 32)
  @IsOptional()
  lastName: string;
}
