import { IsEmail, Length, Matches } from 'class-validator';
import { Field, InputType } from 'type-graphql';
import { User } from '../models/user.model';

@InputType({ description: 'Data to create a user.' })
export class CreateUserInput implements Partial<User> {
  @Field(() => String)
  @Length(1, 32)
  firstName: string;

  @Field(() => String)
  @Length(1, 32)
  lastName: string;

  @Field(() => String)
  @IsEmail()
  email: string;

  @Field(() => String, {
    description:
      "User's password, must contain at least one letter, one number, one symbol, and be at least 8 characters long.",
  })
  @Matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/)
  password: string;
}
