import { Arg, Authorized, Mutation, Query, Resolver } from 'type-graphql';
import MutationResponse from '../../shared/mutation.response';
import PaginatedResponse from '../../shared/paginated.response';
import { PaginationInput } from '../../shared/pagination.input';
import { CreateUserInput } from '../inputs/user.create';
import { UpdateUserInput } from '../inputs/user.update';
import { User } from '../models/user.model';

const PaginatedUserResponse = PaginatedResponse(User);
type PaginatedUserResponse = InstanceType<typeof PaginatedUserResponse>;

const UserMutationResponse = MutationResponse(User);
type UserMutationResponse = InstanceType<typeof UserMutationResponse>;

@Resolver()
export class UserResolvers {
  @Authorized()
  @Query(() => PaginatedUserResponse)
  public async getUsers(
    @Arg('pagination', { nullable: true }) pagination?: PaginationInput
  ): Promise<PaginatedUserResponse> {
    const [items, total] = await User.findPaginated(pagination);
    const next = items.length > 0 ? (items[items.length - 1].id || 0) + 1 : undefined;
    return {
      items,
      total,
      take: pagination?.take,
      next,
    };
  }

  @Authorized()
  @Mutation(() => UserMutationResponse)
  public async createUser(@Arg('data') data: CreateUserInput): Promise<UserMutationResponse> {
    const exists = await User.findOne({ email: data.email });
    if (exists) {
      return {
        success: false,
        message: 'A user with the provided email already exists.',
      };
    }

    try {
      return {
        data: await User.createUser({ ...data }).save(),
        success: true,
        message: 'User creation successful.',
      };
    } catch (e) {
      return {
        success: false,
        message: e.message,
      };
    }
  }

  @Authorized()
  @Mutation(() => UserMutationResponse)
  public async updateUser(@Arg('id') id: number, @Arg('data') data: UpdateUserInput): Promise<UserMutationResponse> {
    const user = await User.findOne(id);
    if (!user) {
      return {
        success: false,
        message: 'A user with the provided id could not be found.',
      };
    }

    const updatedData = {
      ...user,
      ...data,
    };

    try {
      await User.update(id, updatedData);
      return {
        data: updatedData,
        success: true,
        message: 'User update successful.',
      };
    } catch (e) {
      return {
        success: false,
        message: e.message,
      };
    }
  }

  @Authorized()
  @Mutation(() => UserMutationResponse)
  public async deleteUser(@Arg('id') id: number): Promise<UserMutationResponse> {
    const exists = await User.findOne(id);
    if (!exists) {
      return {
        success: false,
        message: 'A user with the provided id could not be found.',
      };
    }

    try {
      await User.softRemove(exists);
      return {
        success: true,
        message: 'User deleted successfully.',
      };
    } catch (e) {
      return {
        success: false,
        message: e.message,
      };
    }
  }
}
