import { Arg, Mutation, Resolver } from 'type-graphql';
import MutationResponse from '../../shared/mutation.response';
import { AccessToken } from '../models/jwt.model';
import { User } from '../models/user.model';

const LoginMutationResponse = MutationResponse(AccessToken);
type LoginMutationResponse = InstanceType<typeof LoginMutationResponse>;

@Resolver()
export class AuthResolvers {
  @Mutation(() => LoginMutationResponse, {
    description:
      'Get an access JWT. After a successful login, include the access token in the Authorization Header, prefixed by "Bearer ".',
  })
  public async login(@Arg('email') email: string, @Arg('password') password: string): Promise<LoginMutationResponse> {
    const exists = await User.findOne({ where: { email }, select: ['id', 'email', 'password'] });
    if (!exists) {
      return {
        success: false,
        message: 'The email provided was not found on our records. Please ask an administrator to provide an account.',
      };
    }

    if (!(await User.verifiyPassword(password, exists.password))) {
      return {
        success: false,
        message: 'The email/password combination provided is incorrect.',
      };
    }

    try {
      const [accessToken, expiration] = exists.generateToken();
      exists.updateLastLogin();
      return {
        data: {
          accessToken,
          expiration,
        },
        success: true,
        message: 'Login successful.',
      };
    } catch (e) {
      return {
        success: false,
        message: e.message,
      };
    }
  }
}
