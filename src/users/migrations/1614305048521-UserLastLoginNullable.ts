import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserLastLoginNullable1614305048521 implements MigrationInterface {
  name = 'UserLastLoginNullable1614305048521';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "lastLogin" DROP NOT NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "user"."lastLogin" IS NULL`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`COMMENT ON COLUMN "user"."lastLogin" IS NULL`);
    await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "lastLogin" SET NOT NULL`);
  }
}
