import { Field, Int, ObjectType } from 'type-graphql';

@ObjectType()
export class AccessToken {
  @Field(() => String, { description: 'JWT token.' })
  accessToken: string;

  @Field(() => Int, { description: 'Seconds until token expiration. ' })
  expiration: number;
}
