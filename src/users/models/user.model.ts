import { ApolloError } from 'apollo-server-express';
import { compare, hashSync } from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { Field, ID, ObjectType } from 'type-graphql';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeepPartial,
  DeleteDateColumn,
  Entity,
  MoreThanOrEqual,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { getJWTSecret, jwtOptions } from '../../auth';
import { PaginationInput } from '../../shared/pagination.input';

@ObjectType()
@Entity()
export class User extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => String, { description: "User's name." })
  @Column({ length: 32 })
  firstName: string;

  @Field(() => String, { description: "User's family name." })
  @Column({ length: 32 })
  lastName: string;

  @Field(() => String, { description: 'Email used for notifications and authentication.' })
  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Field(() => Date, { description: "Timestamp for user's last login." })
  @Column({ nullable: true })
  lastLogin: Date;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  public static createUser(entityLike: DeepPartial<User>): User {
    const password = hashSync(entityLike.password, +(process.env.SALT_ROUNDS || 10));
    return User.create({ ...entityLike, password });
  }

  public static async verifiyPassword(password: string, encrypted: string): Promise<boolean> {
    return compare(password, encrypted);
  }

  public generateToken(): [string, number] {
    if (!process.env.JWT_SECRET) throw new ApolloError('Misconfigured server');
    const accessToken = jwt.sign({ sub: this.id }, getJWTSecret(), jwtOptions());
    return [accessToken, 24 * 60 * 60];
  }

  public async updateLastLogin(): Promise<void> {
    await User.update(this.id, { lastLogin: new Date() });
  }

  public static async findPaginated(pagination?: PaginationInput): Promise<[DeepPartial<User>[], number]> {
    return User.findAndCount({
      where: {
        id: MoreThanOrEqual(pagination?.cursor || 0),
      },
      take: pagination?.take,
    });
  }

  public static async getEmails(): Promise<string[]> {
    const users = await User.find({ select: ['email'] });
    return users.map((u) => u.email);
  }
}
