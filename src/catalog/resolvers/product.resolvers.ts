import DataLoader from 'dataloader';
import { Arg, Authorized, Ctx, FieldResolver, Mutation, Query, Resolver, Root } from 'type-graphql';
import { Loader } from 'type-graphql-dataloader';
import MutationResponse from '../../shared/mutation.response';
import PaginatedResponse from '../../shared/paginated.response';
import { PaginationInput } from '../../shared/pagination.input';
import { ProductFilters } from '../inputs/product.filters';
import { CreateProductInput } from '../inputs/product.create';
import { Brand } from '../models/brand.model';
import { Category } from '../models/category.model';
import { Product } from '../models/product.model';
import { UpdateProductInput } from '../inputs/product.update';
import { DeepPartial, getRepository, Not } from 'typeorm';
import { ApolloError } from 'apollo-server-express';
import { JWTToken } from '../../auth';
import { Logger } from '../../shared/logger.service';

const PaginatedProductResponse = PaginatedResponse(Product);
type PaginatedProductResponse = InstanceType<typeof PaginatedProductResponse>;

const ProductMutationResponse = MutationResponse(Product);
type ProductMutationResponse = InstanceType<typeof ProductMutationResponse>;

@Resolver(() => Product)
export class ProductResolver {
  @FieldResolver(() => [Category])
  @Loader<number, Category[]>(async (ids) => Product.relationBatchLoader(ids, 'categories'))
  public async categories(@Root() parent: Product): Promise<unknown> {
    return (dataloader: DataLoader<number, Category[]>) => dataloader.load(parent.id);
  }

  @FieldResolver(() => Brand)
  @Loader<number, Brand>(async (ids) => Product.relationBatchLoader(ids, 'brand'))
  public async brand(@Root() parent: Product): Promise<unknown> {
    return (dataloader: DataLoader<number, Brand>) => dataloader.load(parent.id);
  }

  @FieldResolver(() => [Product])
  public async relatedProducts(
    @Root() parent: Product,
    @Arg('max', { defaultValue: 5 }) max: number
  ): Promise<DeepPartial<Product>[]> {
    const categories = parent.categories || (await Category.findByProductId(parent.id));
    const [related] = await Product.filter({
      filters: { categories: categories.map((cat) => cat.id) },
      pagination: { take: max },
    });
    return related.filter((p) => p.id !== parent.id);
  }

  @Query(() => Product)
  public async getProduct(@Ctx() { user }: { user: JWTToken }, @Arg('id') id: number): Promise<Product> {
    const product = await Product.findOne(id);
    if (!product) throw new ApolloError(`Unable to find product with id ${id}.`);
    if (!user) {
      // Request is by anonymous user
      await Logger.logAction('query', 'product', id);
    }
    return product;
  }

  @Query(() => PaginatedProductResponse)
  public async getProducts(
    @Arg('pagination', { nullable: true }) pagination?: PaginationInput,
    @Arg('filters', { nullable: true }) filters?: ProductFilters
  ): Promise<PaginatedProductResponse> {
    const [items, total] = await Product.filter({
      filters,
      pagination,
    });
    const next = items.length > 0 ? (items[items.length - 1].id || 0) + 1 : undefined;
    return {
      items,
      total,
      next,
      take: pagination?.take,
    };
  }

  @Authorized()
  @Mutation(() => ProductMutationResponse)
  public async createProduct(@Arg('data') data: CreateProductInput): Promise<ProductMutationResponse> {
    const existing = await Product.findOne({ sku: data.sku });
    if (existing) {
      return {
        success: false,
        message: 'A product with the provided sku already exists.',
      };
    }

    try {
      return {
        data: await Product.create({
          ...data,
          brand: { id: data.brandId },
          categories: data.categoriesIds?.map((id) => {
            return { id };
          }),
        }).save(),
        success: true,
        message: 'Product creation successful.',
      };
    } catch (e) {
      return {
        success: false,
        message: e.message,
      };
    }
  }

  @Authorized()
  @Mutation(() => ProductMutationResponse)
  public async updateProduct(
    @Arg('id') id: number,
    @Arg('data') data: UpdateProductInput
  ): Promise<ProductMutationResponse> {
    const product = await Product.findOne(id);
    if (!product) {
      return {
        success: false,
        message: 'A product with the provided id could not be found.',
      };
    }

    if (data.sku) {
      const existing = await Product.findOne({ id: Not(id), sku: data.sku });
      if (existing) {
        return {
          success: false,
          message: 'A product with the provided sku already exists.',
        };
      }
    }

    const updatedData = {
      ...product,
      ...data,
    };

    try {
      await getRepository(Product).update(id, updatedData);
      return {
        data: updatedData,
        success: true,
        message: 'Product update successful.',
      };
    } catch (e) {
      return {
        success: false,
        message: e.message,
      };
    }
  }

  @Authorized()
  @Mutation(() => ProductMutationResponse)
  public async deleteProduct(@Arg('id') id: number): Promise<ProductMutationResponse> {
    const exists = await Product.findOne(id);
    if (!exists) {
      return {
        success: false,
        message: 'A product with the provided id could not be found.',
      };
    }

    try {
      await Product.softRemove(exists);
      return {
        success: true,
        message: 'Product deleted successfully.',
      };
    } catch (e) {
      return {
        success: false,
        message: e.message,
      };
    }
  }
}
