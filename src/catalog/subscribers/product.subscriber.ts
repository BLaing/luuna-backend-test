import { EntitySubscriberInterface, EventSubscriber, InsertEvent, RemoveEvent, UpdateEvent } from 'typeorm';
import { NotificationService } from '../../shared/notifications.service';
import { User } from '../../users/models/user.model';
import { Product } from '../models/product.model';

@EventSubscriber()
export class ProductSubscriber implements EntitySubscriberInterface<Product> {
  private static async notifyAction(action: 'insert' | 'update' | 'delete', entity?: Product): Promise<void> {
    let message = '';
    let subject = '';
    switch (action) {
      case 'insert':
        subject = 'New Product registered!';
        message = `A new product (${entity?.id}) named ${entity?.name} has been created.`;
        break;
      case 'update':
        if (!entity?.deletedAt) {
          subject = 'Product updated!';
          message = `Product ${entity?.id} has ben modified`;
          break;
        }
        subject = 'Product deleted!';
        message = `Product ${entity?.id} has been deleted.`;
        break;
      case 'delete':
        subject = 'Product deleted!';
        message = `Product ${entity?.id} has been deleted.`;
        break;
    }
    await NotificationService.sendEmail(await User.getEmails(), message, subject);
  }

  listenTo(): { new (): Product } {
    return Product;
  }

  async afterInsert(event: InsertEvent<Product>): Promise<void> {
    await ProductSubscriber.notifyAction('insert', event.entity);
  }

  async afterUpdate(event: UpdateEvent<Product>): Promise<void> {
    await ProductSubscriber.notifyAction('update', event.entity);
  }

  async afterRemove(event: RemoveEvent<Product>): Promise<void> {
    await ProductSubscriber.notifyAction('update', event.entity);
  }
}
