import { In, MigrationInterface, QueryRunner } from 'typeorm';

export class ProductSeeds1614351040655 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const brandRepo = queryRunner.manager.getRepository('brand');
    const categoryRepo = queryRunner.manager.getRepository('category');
    const productRepo = queryRunner.manager.getRepository('product');
    try {
      const brands = await brandRepo.save([{ name: 'Luuna' }, { name: 'Nooz' }]);
      const categories = await categoryRepo.save([{ name: 'Colchones' }, { name: 'Muebles' }, { name: 'Almohadas' }]);

      await productRepo.save([
        { name: 'Colchón Luuna One', price: 15199, sku: '1', brand: brands[0], categories: [categories[0]] },
        {
          name: 'Colchón Luuna Blue',
          price: 6499,
          sku: '2',
          brand: brands[0],
          categories: [categories[0], categories[1]],
        },
        { name: 'Almohada Luxe', price: 799, sku: '3', brand: brands[1], categories: [categories[2]] },
      ]);
    } catch (e) {
      console.log('Could not execute ProductSeeds Migration.');
      console.error(e);
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const brandRepo = queryRunner.manager.getRepository('brand');
    const categoryRepo = queryRunner.manager.getRepository('category');
    const productRepo = queryRunner.manager.getRepository('product');

    try {
      const brands = await brandRepo.find({ where: { name: In(['Luuna', 'Nooz']) } });
      const categories = await categoryRepo.find({ where: { name: In(['Colchones', 'Muebles', 'Almohadas']) } });
      const products = await productRepo.find({ where: { sku: In(['1', '2', '3']) } });

      await productRepo.delete(products);
      await categoryRepo.delete(categories);
      await brandRepo.delete(brands);
    } catch (e) {
      console.log('Could not tear downw ProductSeeds Migration.');
      console.error(e);
    }
  }
}
