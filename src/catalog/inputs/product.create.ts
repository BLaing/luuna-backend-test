import { IsInt, Length, Min } from 'class-validator';
import { Field, Float, InputType, Int } from 'type-graphql';
import { Product } from '../models/product.model';

@InputType({ description: 'Data to add a product.' })
export class CreateProductInput implements Partial<Product> {
  @Field(() => String)
  @Length(1, 32)
  name: string;

  @Field(() => String)
  @Length(1, 16)
  sku: string;

  @Field(() => Float)
  @Min(0)
  price: number;

  @Field(() => Int)
  @IsInt()
  brandId: number;

  @Field(() => [Int], { nullable: true })
  categoriesIds?: number[];
}
