import { IsInt, IsOptional, Length, Min } from 'class-validator';
import { Field, Float, InputType, Int } from 'type-graphql';
import { Product } from '../models/product.model';

@InputType({ description: 'Data to update a product. Missing values will be ommited' })
export class UpdateProductInput implements Partial<Product> {
  @Field(() => String, { nullable: true })
  @Length(1, 32)
  @IsOptional()
  name?: string;

  @Field(() => String, { nullable: true })
  @Length(1, 16)
  @IsOptional()
  sku?: string;

  @Field(() => Float, { nullable: true })
  @Min(0)
  @IsOptional()
  price?: number;

  @Field(() => Int, { nullable: true })
  @IsInt()
  @IsOptional()
  brandId?: number;

  @Field(() => [Int], { nullable: true })
  @IsOptional()
  categoriesIds?: number[];
}
