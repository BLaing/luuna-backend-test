import { Field, InputType, Int } from 'type-graphql';

@InputType({ description: 'Optional values used to filter products.' })
export class ProductFilters {
  @Field(() => [Int], { description: 'List of brand IDs.', nullable: true })
  brands?: number[];

  @Field(() => [Int], { description: 'List of category IDs.', nullable: true })
  categories?: number[];

  @Field(() => [String], { description: 'List of SKUs to include in results.', nullable: true })
  skus?: string[];
}
