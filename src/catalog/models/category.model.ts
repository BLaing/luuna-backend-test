import { Field, ID, ObjectType } from 'type-graphql';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeepPartial,
  DeleteDateColumn,
  Entity,
  ManyToMany,
  PrimaryGeneratedColumn,
  SelectQueryBuilder,
  UpdateDateColumn,
} from 'typeorm';
import { Product } from './product.model';

@ObjectType()
@Entity()
export class Category extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => String, { description: 'Customer-friendly name' })
  @Column({ length: 32 })
  name: string;

  @Field(() => [Product], { description: 'List of products belonging to a category.' })
  @ManyToMany(() => Product, (product) => product.categories)
  products: Product[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  public static async findByProductId(id: number): Promise<DeepPartial<Category>[]> {
    const query: SelectQueryBuilder<Category> = Category.createQueryBuilder('category').innerJoin(
      'category.products',
      'product',
      'product.id = :id',
      { id }
    );
    try {
      return query.getMany();
    } catch {
      return [];
    }
  }
}
