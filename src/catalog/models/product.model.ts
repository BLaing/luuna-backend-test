import { Field, Float, ID, ObjectType } from 'type-graphql';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeepPartial,
  DeleteDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  SelectQueryBuilder,
  UpdateDateColumn,
} from 'typeorm';
import { PaginationInput } from '../../shared/pagination.input';
import { ProductFilters } from '../inputs/product.filters';
import { Brand } from './brand.model';
import { Category } from './category.model';

@ObjectType()
@Entity()
export class Product extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => String, { description: 'Product identifier used for stock traceability.' })
  @Column({ unique: true, length: 16 })
  sku: string;

  @Field(() => String, { description: 'Customer-friendly name.' })
  @Column({ length: 32 })
  name: string;

  @Field(() => Float, { description: 'Current retail price of the product, taxes included.' })
  @Column({ type: 'float' })
  price: number;

  @Field(() => Brand, { description: 'Brand to which the product belongs.' })
  @ManyToOne(() => Brand, (brand) => brand.products)
  brand: Brand;

  @Field(() => [Category], { description: 'List of categories related to the product.' })
  @ManyToMany(() => Category, (category) => category.products)
  @JoinTable()
  categories: Category[];

  @Field(() => [Product], { description: 'List of related products which could be of interest to the user.' })
  relatedProducts: Product[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  public static async relationBatchLoader<T>(keys: readonly number[], relation: keyof Product): Promise<T[]> {
    const products = await Product.findByIds([...keys], { relations: [relation] });
    if (products.length === 0)
      return keys.map(() => {
        return {} as T;
      });
    return keys.map((key) => {
      const product = products.find(({ id }) => id === key);
      return product ? ((product[relation] as unknown) as T) : ({} as T);
    });
  }

  public static async filter(
    filterOptions: { filters?: ProductFilters; pagination?: PaginationInput } = {}
  ): Promise<[DeepPartial<Product>[], number]> {
    let query: SelectQueryBuilder<Product> = Product.createQueryBuilder('product');

    query.where('product.deletedAt IS NULL');
    if (filterOptions.pagination?.cursor) {
      query = query.andWhere('product.id >= :cursor', { cursor: filterOptions.pagination.cursor });
    }

    if (filterOptions.filters?.brands) {
      query = query.innerJoin('product.brand', 'brand', 'brand.id IN(:...brands)', {
        brands: filterOptions.filters.brands,
      });
    }

    if (filterOptions.filters?.categories) {
      query = query.innerJoin('product.categories', 'category', 'category.id IN(:...categories)', {
        categories: filterOptions.filters.categories,
      });
    }

    if (filterOptions.filters?.skus) {
      query = query.andWhere('product.sku IN(:...skus)', { skus: filterOptions.filters.skus });
    }

    query.orderBy('product.id', 'ASC').take(filterOptions.pagination?.take);

    try {
      return await query.getManyAndCount();
    } catch {
      return [[], 0];
    }
  }
}
