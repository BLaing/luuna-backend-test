import { ApolloError } from 'apollo-server-express';
import { verify, SignOptions } from 'jsonwebtoken';
import { AuthChecker } from 'type-graphql';

export interface JWTToken {
  iss: string;
  sub: number;
}

export const jwtOptions = (): SignOptions => {
  return {
    issuer: process.env.JWT_ISSUER,
    algorithm: 'HS256',
    expiresIn: '24h',
  };
};

export const getJWTSecret = (): string => {
  if (!process.env.JWT_SECRET) throw new ApolloError('Misconfigured server');
  return process.env.JWT_SECRET;
};

export const getUserFromJWT = async (token: string): Promise<JWTToken> => {
  if (!token || token === '') {
    throw new ApolloError('Unauthorized');
  }

  try {
    const decoded = verify(token, getJWTSecret(), jwtOptions());
    return decoded as JWTToken;
  } catch {
    throw new ApolloError('Unauthorized: Invalid JWT');
  }
};

export const authChecker: AuthChecker<{ user: JWTToken }> = ({ context: { user } }) => {
  if (!user) return false;
  return true;
};
