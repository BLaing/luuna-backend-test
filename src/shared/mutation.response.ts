import { ClassType, Field, ObjectType } from 'type-graphql';

export default function MutationResponse<T>(TClass: ClassType<T>): ClassType {
  @ObjectType(`${TClass.name}MutationResponse`)
  class MutationResponseClass {
    @Field(() => TClass, { description: `Object of type ${TClass.name}.`, nullable: true })
    data?: T;

    @Field(() => Boolean, { description: 'Wether or not the requested mutation was successful.' })
    success: boolean;

    @Field(() => String, {
      description: 'Additional information regarding the rersult of the mutation.',
      nullable: true,
    })
    message?: string;
  }

  return MutationResponseClass;
}
