import * as sendgrid from '@sendgrid/mail';

export class NotificationService {
  private static isReady = false;
  private static sender: string;
  public static configure(apiKey: string, sender: string): void {
    sendgrid.setApiKey(apiKey);
    NotificationService.sender = sender;
    NotificationService.isReady = true;
  }

  public static async sendEmail(recipient: string | string[], content: string, subject: string): Promise<boolean> {
    if (!NotificationService.isReady) return false;
    try {
      await sendgrid.send({
        to: recipient,
        from: NotificationService.sender,
        subject,
        text: content,
      });
      return true;
    } catch (e) {
      return false;
    }
  }
}
