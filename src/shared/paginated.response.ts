import { ClassType, Field, Int, ObjectType } from 'type-graphql';

export default function PaginatedResponse<T>(TClass: ClassType<T>): ClassType {
  @ObjectType(`Paginated${TClass.name}Response`)
  class PaginatedResponseClass {
    @Field(() => [TClass], { description: `Array of ${TClass.name} elements.` })
    items: T[];

    @Field(() => Int, { description: 'Total amount of elements with the selected filters.' })
    total: number;

    @Field(() => Int, { description: 'Amount of elements selected to take from total.', nullable: true })
    take?: number;

    @Field(() => Int, { description: `Cursor to get the next page of ${TClass.name} elements.`, nullable: true })
    next?: number;
  }

  return PaginatedResponseClass;
}
