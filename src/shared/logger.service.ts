import { Client } from '@elastic/elasticsearch';

export class Logger {
  private static client: Client;

  private static getClient(): Client {
    Logger.client =
      Logger.client ||
      new Client({
        node: process.env.ELASTIC_NODE,
        maxRetries: 5,
        requestTimeout: 60000,
        sniffOnStart: false,
        auth: {
          apiKey: {
            id: process.env.ELASTIC_KEY_ID ?? '',
            api_key: process.env.ELASTIC_API_KEY ?? '',
          },
        },
      });

    return Logger.client;
  }
  public static async logAction(action: string, resource: string, id: number): Promise<boolean> {
    try {
      await Logger.getClient().index({
        index: `product-catalog-queries`,
        body: {
          action,
          resource,
          id,
          timestamp: new Date(),
        },
      });
      return true;
    } catch (e) {
      return false;
    }
  }
}
