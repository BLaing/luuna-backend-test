import { Field, InputType, Int } from 'type-graphql';

@InputType({
  description: 'Generic input for pagination purposes. Field used as cursor can vary according to each Object Type.',
})
export class PaginationInput {
  @Field(() => Int, { nullable: true, description: 'First value to retrieve.' })
  cursor?: number;

  @Field(() => Int, {
    nullable: true,
    defaultValue: 25,
    description: 'Maximun amount of elements to take. Defaults to 25.',
  })
  take?: number;
}
